#!/usr/bin/env python
# Copyright (c) 2018 errol project
# This code is distributed under the GPLv3 License
# The code is inspired from https://github.com/rbarrois/aionotify

from setuptools import setup, find_packages
from codecs import open
from os import path

here = path.abspath(path.dirname(__file__))

try:
    import pypandoc

    long_description = pypandoc.convert_file(path.join(here, 'README.md'), 'rst')
except(IOError, ImportError):
    long_description = open(path.join(here, 'README.md'), encoding='utf-8').read()

# Arguments marked as "Required" below must be included for upload to PyPI.
# Fields marked as "Optional" may be commented out.

setup(
    name='errol',
    version='2.1.0',
    description='Errol is a file sender that rely on inotify. It can be used to watch a directory and automatically transfer the new files (or modified ones) with XMPP.',
    long_description=long_description,
    long_description_content_type='text/x-rst',
    url='https://gitlab.com/jnanar/errol',
    author='Arnaud Joset',
    author_email='info@agayon.be',
    classifiers=[
        # https://pypi.python.org/pypi?%3Aaction=list_classifiers
        'Development Status :: 3 - Alpha',
        'Environment :: Console',
        'Intended Audience :: Developers',
        'Topic :: Internet :: XMPP',
        'Topic :: Communications',
        'Topic :: Communications :: File Sharing',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Programming Language :: Python :: 3 :: Only',
        'Operating System :: POSIX :: Linux',
    ],
    keywords='xmpp file sharing watchdoc',
    packages=find_packages(exclude=[]),
    install_requires=['watchdog', 'slixmpp', 'hachiko'],
    python_requires='>=3.6',
    # If there are data files included in your packages that need to be
    # installed, specify them here.
    package_data={
        'errol': ['data/template.xml'],
    },
    # To provide executable scripts, use entry points in preference to the
    # "scripts" keyword. Entry points provide cross-platform support and allow
    # `pip` to create the appropriate form of executable for the target
    # platform.
    #
    entry_points={  # Optional
        'console_scripts': [
            'errol=errol.errol_launcher:launcher',
        ],
    },
)
